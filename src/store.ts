import { writable } from "svelte/store"
import type { Civ } from "./Civ"

export const modifiedCivs = writable(new Map<string, Civ>())

modifiedCivs.subscribe((map) => console.log(map))

export const currentPage = writable<"EDITOR" | "INFO">(
  /#?editor/i.test(window.location.hash) ? "EDITOR" : "INFO",
)
