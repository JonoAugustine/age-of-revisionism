module.exports = {
  publicPath:
    process.env.NODE_ENV === "PRODUCTION" ? process.env.PUBLIC_PATH : "/",
}
